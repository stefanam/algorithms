package com.stefana.backtracking.nqueensproblem;

public class Maze {

    private int [][] mazeTable;
    private int[][] solutionTable;
    private int mazeSize;

    public Maze(int[][] mazeTable) {
        this.mazeTable = mazeTable;
        this.mazeSize = mazeTable.length;
        this.solutionTable = new int[mazeSize][mazeSize];
    }

    public void solve(){
        if (solveMaze(0, 0)){
            showResults();
        } else {
            System.out.println("No solution...");
        }
    }

    private boolean solveMaze(int x, int y) {
        if (isFinished(x, y)){
            return true;
        }

        if (isValid(x, y)){
            solutionTable[x][y] = 1;


            if (solveMaze(x + 1, y)){  //ovde treba dodati i varijantu sa x - 1, y i x, y-1
                                         // da bi mogao da se krece u suprotnu stranu (ovde moze samo levo i gore da ide)
                return true;
            }

            if (solveMaze(x, y + 1)){
                return true;
            }

            //Backtrack!

            solutionTable[x][y] = 0;
        }

        return false;
    }

    private boolean isValid(int x, int y) {
        if (x < 0 || x >= mazeSize) return false;
        if (y < 0 || y >= mazeSize) return false;

        if (mazeTable[x][y] != 1) return false;
        //ako je na tom polju 0 to znaci da je to prepreka i da ne mozemo da idemo u tom pravcu

        return true;
    }

    private boolean isFinished(int x, int y) {
        if (x == mazeSize - 1 && y == mazeSize - 1){  //proveravamo da li smo stigli do poslednjeg polja u tabli
            solutionTable[x][y] = 1;  //da bi to polje ukljucili u resenje
            return true;
        }
        return false;
    }

    private void showResults(){
        for (int i = 0; i < mazeSize; i++){
            for (int j = 0; j < mazeSize; j++){
                if (solutionTable[i][j] == 1){
                    System.out.print(" S ");
                } else {
                    System.out.print(" - ");
                }
            }

            System.out.println();
        }
    }
}
