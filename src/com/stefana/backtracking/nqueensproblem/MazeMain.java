package com.stefana.backtracking.nqueensproblem;

public class MazeMain {

    public static void main(String[] args) {

        int [][] maze = {
                {1, 1, 1, 1, 1},
                {1, 0, 0, 1, 0},
                {0, 0, 0, 1, 0},
                {1, 1, 1, 1, 1},
                {1, 1, 1, 0, 1}
        };

        Maze maze1 = new Maze(maze);
        maze1.solve();
    }
}
