package com.stefana.backtracking.nqueensproblem;

public class QueensProblem {

    private int chessTable[][];
    private int numOfQueens;

    public QueensProblem(int numOfQueens) {
        this.chessTable = new int[numOfQueens][numOfQueens];
        this.numOfQueens = numOfQueens;
    }

    public void solve(){
        if (setQueens(0)){
            printQueens();
        } else {
            System.out.println("There is no solution...");
        }
    }

    private boolean setQueens(int columnIndex) {
        if (columnIndex == numOfQueens){  //that means that we placed all the queens on the chess table
            return true;                 //so it returns true and we can print queens
        }

        for (int rowIndex = 0; rowIndex < numOfQueens; rowIndex++){

            if (isPlaceValid(rowIndex, columnIndex)){ //place is valid

                chessTable[rowIndex][columnIndex] = 1;  //so we PUT that queen on chess table

                if (setQueens(columnIndex + 1)){  //if we can place queen in the next column we return true
                                                //and we call this method recursively
                                               //until all of the queens are placed on chess table
                    return true;
                }

                //in this case we are not able to find proper place for next queen
                //BACKTRACKING!!!
                chessTable[rowIndex][columnIndex] = 0;  //moving queen from that column
                                                       //and move back to for loop to try next row available
            }
        }
        return false;
        //when we call false that is when setQueens(colIndex + 1) is false so we have to BACKTRACK!
    }

    private boolean isPlaceValid(int rowIndex, int columnIndex) {

        for (int i = 0; i < chessTable.length; i++){
            if (chessTable[rowIndex][i] == 1){
                return false;
            }
        }

        //we check if there is any queens in diagonal
        for (int i = rowIndex, j = columnIndex; i >= 0 && j >= 0; i--, j--){
            if (chessTable[i][j] == 1){
                return false;   //there is a queen in that diagonal so we have to return false
            }
        }
        //checking other diagonal
        for (int i = rowIndex, j = columnIndex; i < chessTable.length && j >= 0; i++, j--){
            if (chessTable[i][j] == 1){   //there is a queen in that diagonal so we have to return false

                return false;
            }
        }
        return true;
    }

    public void printQueens() {
        for (int i = 0; i < chessTable.length; i++){
         for (int j = 0; j < chessTable.length; j++){
             if (chessTable[i][j] == 1){   //if queen is at that place put *
                 System.out.print(" * ");
             } else {
                 System.out.print(" - ");  //if not put -
             }
         }

            System.out.println();
        }

    }
}
