package com.stefana.backtracking.nqueensproblem;

public class QuinsProblemMain {

    public static void main(String[] args) {

        QueensProblem queensProblem = new QueensProblem(100);
        queensProblem.solve();
    }
}
