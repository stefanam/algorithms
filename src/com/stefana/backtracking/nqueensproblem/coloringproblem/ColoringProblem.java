package com.stefana.backtracking.nqueensproblem.coloringproblem;

public class ColoringProblem {

    private int numOfVertexex;
    private int numOfColors;
    private int[] colors;
    private int[][] adjMatrix;

    public ColoringProblem(int numOfColors, int[][] adjMatrix) {
        this.numOfColors = numOfColors;
        this.adjMatrix = adjMatrix;
        this.numOfVertexex = adjMatrix.length;
        this.colors = new int[numOfVertexex];


    }

    public void solve(){
        if (solveProblem(0)){
            showResults();
        } else {
            System.out.println("No solution...");
        }
    }

    private boolean solveProblem(int nodeIndex) {
        if (nodeIndex == numOfVertexex){
            return true;
        }

        for (int colorIndex = 1; colorIndex <= numOfColors; colorIndex++){
            if (isColorValid(nodeIndex, colorIndex)){
                colors[nodeIndex] = colorIndex;

                if (solveProblem(nodeIndex + 1)){  //ako moze da se pozove opet za sl node
                    //pozvace se onoliko puta koliko nodova imamo
                    return true;
                }

                //Backtrack!!!
            }
        }
        return false;
    }

    private boolean isColorValid(int nodeIndex, int colorIndex) {
        for (int i = 0; i < numOfVertexex; i++){
            if (adjMatrix[nodeIndex][i] == 1 && colorIndex == colors[i]){  //naisao je na istu boju
                return false;
            }
        }
        return true;  // mozemo da dodelimo tu odredjenu boju tom odredjenom nodu
    }

    private void showResults() {
        for (int i = 0; i < numOfVertexex; i++){
            System.out.println("Node " + (i + 1) + " has color index " + colors[i]);
        }
    }
}
