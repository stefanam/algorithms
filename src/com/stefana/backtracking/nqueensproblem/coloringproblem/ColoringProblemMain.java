package com.stefana.backtracking.nqueensproblem.coloringproblem;

public class ColoringProblemMain {

    public static void main(String[] args) {

        int[][] matrix = new int[][]{
                {0, 1, 0, 1, 0},
                {1, 0, 1, 1, 0},
                {0, 1, 0, 1, 0},
                {1, 1, 1, 0, 1},
                {0, 0, 0, 1, 0}
        };


        int numOfColors = 3;
        ColoringProblem coloringProblem = new ColoringProblem(numOfColors, matrix);
        coloringProblem.solve();
    }
}
