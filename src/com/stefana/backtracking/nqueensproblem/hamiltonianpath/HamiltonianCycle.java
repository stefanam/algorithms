package com.stefana.backtracking.nqueensproblem.hamiltonianpath;

public class HamiltonianCycle {

    private int numOfVertexex;
    private int[] hamiltonianPath;
    private int[][] adjMatrix;

    public HamiltonianCycle(int[][] adjMatrix) {
        this.adjMatrix = adjMatrix;
        this.numOfVertexex = adjMatrix.length;
        this.hamiltonianPath = new int[adjMatrix.length];

        this.hamiltonianPath[0] = 0;

    }

    public void solve(){

        if (findFeasibleSolution(1)){
            showHamiltonPath();
        } else {
            System.out.println("No feasible solution...");
        }
    }

    private boolean findFeasibleSolution(int position) {
        if (position == numOfVertexex){
            if (adjMatrix[hamiltonianPath[position - 1]][hamiltonianPath[0]] == 1){
                //da li je poslednji element povezan sa prvim elementom
                return true;
            } else {
                return false;
            }
        }

        for (int vertexIndex = 0; vertexIndex < numOfVertexex; vertexIndex++){
            if (isFeasible(vertexIndex, position)){
                hamiltonianPath[position] = vertexIndex;

                if (findFeasibleSolution(position + 1)){
                    //pozivamo rekurzivno u zavisnosti od broja vertexa
                    return true;   //vracamo true ako mozemo da nadjemo sledeci vertex na hamiliton putu
                }

                //Backtrack!!!
            }
        }
        return false;
    }

    private boolean isFeasible(int vertexIndex, int position) {
        //are two nodes conected?
        if (adjMatrix[hamiltonianPath[position - 1]][vertexIndex] == 0){
            return false;
        }

        for (int i = 0; i < position; i++){
            if (hamiltonianPath[i] == vertexIndex){
                return false;
            }
        }
        return true;
    }

    private void showHamiltonPath() {
        System.out.print("Hamiltonian cycle: ");
        for (int i = 0; i < hamiltonianPath.length; i++){
            System.out.println(hamiltonianPath[i] + " ");
        }

        System.out.println(hamiltonianPath[0]);
    }
}
