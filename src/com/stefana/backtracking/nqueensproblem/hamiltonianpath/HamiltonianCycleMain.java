package com.stefana.backtracking.nqueensproblem.hamiltonianpath;

public class HamiltonianCycleMain {

    public static void main(String[] args) {

        int matrix[][] = {
                {0, 1, 0},
                {1, 0, 1},
                {0, 1, 0}
        };
        HamiltonianCycle hamiltonianCycle = new HamiltonianCycle(matrix);
        hamiltonianCycle.solve();
    }
}
