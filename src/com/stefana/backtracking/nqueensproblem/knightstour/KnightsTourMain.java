package com.stefana.backtracking.nqueensproblem.knightstour;

public class KnightsTourMain {

    public static void main(String[] args) {
        KnightsTour knightsTour = new KnightsTour();
        knightsTour.solveTour();
    }
}
