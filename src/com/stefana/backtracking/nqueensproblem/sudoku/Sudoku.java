package com.stefana.backtracking.nqueensproblem.sudoku;

import com.stefana.backtracking.nqueensproblem.Maze;

import static com.stefana.backtracking.nqueensproblem.knightstour.KnightsTour.BOARD_SIZE;

public class Sudoku {

    private int[][] sudokuTable;

    public static final int BOARD_SIZE = 9;
    public static final int MIN_NUMBER = 1;
    public static final int MAX_NUMBER = 9;
    public static final int BOX_SIZE = 3;

    public Sudoku(int[][] sudokuTable) {
        this.sudokuTable = sudokuTable;
    }

    public void solveProblem() {
        if (!solve(0, 0)) {
            showResults();
        } else {
            System.out.println("No solution...");
        }
    }

    private boolean solve(int rowIndex, int columnIndex) {
        if (rowIndex == BOARD_SIZE && ++columnIndex == BOARD_SIZE){
            return true;   //zavrsili smo, mozemo da prikazemo rezultate
        }

        if (rowIndex == BOARD_SIZE){
            rowIndex = 0;
        }

        if (sudokuTable[rowIndex][columnIndex] != 0){  //prazne celije su oznacene sa brojem 0
            return solve(rowIndex + 1, columnIndex);
        }
            //to znaci da ako nema 0, na tom mestu postoji inicijalizovana vrednost
            //zato ne moramo da razmatramo tu celiju i mozemo da predjemo na sledecu

            for (int number = MIN_NUMBER; number < MAX_NUMBER; number++){
                if (isValid(rowIndex, columnIndex, number)){
                    sudokuTable[rowIndex][columnIndex] = number;
                    //ako je pozicija za taj broj validna mozemo da joj ga dodelimo

                    if (solve(rowIndex + 1, columnIndex)){  //zatim pokusavamo da resimo sledeci red
                        return true;
                    }

                    //Backtrack!

                    sudokuTable[rowIndex][columnIndex] = 0;
                }
            }
        return false;
    }

    private boolean isValid(int rowIndex, int columnIndex, int number) {

        for (int i = 0; i < BOARD_SIZE; i++){  //dati br se vec nalazi u tom redu
            if (sudokuTable[i][rowIndex] == number){
                return false;
            }
        }

        for (int i = 0; i < BOARD_SIZE; i++){  //dati broj se vec nalazi u toj koloni
            if (sudokuTable[columnIndex][i] == number){
                return false;
            }
        }

        //da obezbedimo da nema istog broja u 3 x 3 kucici
        int boxRowOffset = (columnIndex / 3) * BOX_SIZE;
        int boxColumnOffset = (columnIndex / 3) * BOX_SIZE;

        for (int i = 0; i < BOARD_SIZE; i++){
            for (int j = 0; j < BOARD_SIZE; j++){
                if (number == sudokuTable[boxRowOffset + i][boxColumnOffset + j]){
                    return false;
                }

            }
        }
        return true;  //validan potez
    }

    public void showResults() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (i % 3 == 0) System.out.println(" ");
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (j % 3 == 0) System.out.print(" ");
                System.out.print(sudokuTable[i][j] + " ");

            }

            System.out.println();
        }
    }
}
