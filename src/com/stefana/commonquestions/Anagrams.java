package com.stefana.commonquestions;

import java.util.Arrays;

public class Anagrams {

    public static void main(String[] args) {

        System.out.println(isAnagrame("gramana", "anagram"));
        System.out.println(isAnagrame2("grfgana", "anagram"));
        System.out.println(isAnagrameHis("gramana", "anagram"));

    }

    public static boolean isAnagrame(String string1, String string2){
        char[] chars1 = string1.toCharArray();
        char[] chars2 = string2.toCharArray();

        if (chars1.length != chars2.length){
            return false;
        }

        if (string1.equals(string2)){
            return true;
        }

        Arrays.sort(chars1);
        Arrays.sort(chars2);

        String s1 = String.valueOf(chars1);
        String s2 = String.valueOf(chars2);


        if (s1.equals(s2)){
            return true;
        }

        return false;

    }

    public static boolean isAnagrameHis(String string1, String string2){
        char[] chars1 = string1.toCharArray();
        char[] chars2 = string2.toCharArray();

        if (chars1.length != chars2.length){
            return false;
        }

        if (string1.equals(string2)){
            return true;
        }

        Arrays.sort(chars1);
        Arrays.sort(chars2);

        for (int i = 0; i < chars1.length; i++){
            if (chars1[i] != chars2[i]){
                return false;
            }
        }

        return true;

    }

    public static boolean isAnagrame2(String string1, String string2){
        char[] chars1 = string1.toCharArray();
        char[] chars2 = string2.toCharArray();

        int[] brojac1 = new int[60];

        if (chars1.length != chars2.length){
            return false;
        }

        if (string1.equals(string2)){
            return true;
        }

        for (int i = 0; i < chars1.length; i++){
            brojac1[chars1[i] - 'A']++;
        }

        for (int i = 0; i < chars2.length; i++){
            brojac1[chars2[i] - 'A']--;
        }

        for (int i = 0; i < brojac1.length; i++){
            if (brojac1[i] != 0){
                return false;
            }
        }

        return true;
    }
}
