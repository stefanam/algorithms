package com.stefana.commonquestions;

public class Duplicates {

    public static void main(String[] args) {

        int[] niz = {1, 2, 4, 2, 5, 6, 6, 6, 6};
        int[] niz2 = {1, 2, 3, 4, 5};
        int[] niz3 = {3, 3, 3, 3, 5};
        int[] niz4 = {3, 4, 5, 7, 90};
        System.out.println("Is unique: " + isArrayUnique(niz4));
        print(removeDuplicates(niz));
        findRepetitions(niz);



    }

    public static void findRepetitions(int[] niz){

        for (int i = 0; i < niz.length; i++){
            //if this value is a positive we have to flip
            if (niz[Math.abs(niz[i])] > 0){
                niz[Math.abs(niz[i])] = - niz[Math.abs(niz[i])];
                //otherwise it is negative.. that means repetition
            } else {
                System.out.println(Math.abs(niz[i]) + " is a repetition");
            }
        }
    }

    public static boolean isArrayUnique(int[] niz) {

        for (int i = 0; i <= niz.length - 1; i++) {
            for (int j = i + 1; j <= niz.length - 1; j++) {
                if (niz[i] != niz[j]) {

                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public static int[] removeDuplicates(int[] niz){

        int[] noviNiz = new int[niz.length];
        int current = 0;

        for (int i = 0; i <= niz.length - 1; i++) {

            boolean nadjen = false;

            for (int j = i + 1; j <= niz.length - 1; j++) {


                if (niz[i] != niz[j]) {

                } else {

                nadjen = true;
                break;
                }
            }

            if (!nadjen){
                noviNiz[current++] = niz[i];
            }
        }
        return noviNiz;
    }

    public static void print(int[] array){
        for (int i : array){
            System.out.print(i + " ");
        }

        System.out.println();
    }

    public static int[] removeDuplicates2(int[] niz){

        int brDuplikata = 0;
        for (int i = 0; i <= niz.length - 1; i++) {
            for (int j = i+1; j < niz.length; j++) {
                if (niz[i] == niz[j]) {
                    niz[i] = -1;
                    brDuplikata++;
                    break;
                }
            }
        }

        int noviNiz[] = new int[niz.length - brDuplikata];
        int current = 0;
        for (int i = 0; i < niz.length; i++){
            if (niz[i] != -1){
                noviNiz[current++] = niz[i];
            }
        }

        return noviNiz;
    }

}
