package com.stefana.commonquestions;

public class IntegerReversion {

    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4};
        int[] reverse = reverseArray(array);
        print(reverse);

        System.out.println(reverseInteger(5643));

    }

    public static int[] reverseArray(int[] array){

        int[] reversed = new int[array.length];

        for (int i = array.length - 1; i >= 0; i--){
            reversed[array.length - 1 - i] += array[i];
        }

        return reversed;
    }

    public static int reverseInteger(int integer){
        int reversed = 0;
        int remainder = 0;

        while (integer > 0){

            remainder = integer % 10;  /// 1234 % 10 = 4
            integer = integer / 10;   /// integer = 123
            reversed = reversed * 10 + remainder;  /// 0 * 10 + 4 = 4

                                           ///sle iter:  remainder = 123 % 10 = 3; integer = 12; 4 * 10 + 3 = 43
                                          ///sle iter: remainder = 12 % 10 = 2; integer = 1; 43 * 10 + 2 = 432
                                         ///sle iter: remainder = 1 % 10 = 1; integer = 0; 432 * 10 + 1 = 4321
        }

        return reversed;
    }

    public static void print(int[] array){
        for (int i : array){
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
