package com.stefana.commonquestions;

public class LargestSumOfSubarray {

    public static void main(String[] args) {

        int[] niz = {1, -2, 3, 4, -5, 8};
        System.out.println(solve(niz));
    }

    public static int solve(int[] niz){

        int maxGlobal = niz[0];
        int maxCurrent = niz[0];

        for (int i = 1; i < niz.length; i++){
            maxCurrent = Math.max(niz[i], maxCurrent + niz[i]);

            if (maxCurrent > maxGlobal){
                maxGlobal = maxCurrent;
            }
        }

        return maxGlobal;
    }
}
