package com.stefana.commonquestions;

public class Palindrome {

    public static void main(String[] args) {

        String s = "nesto";
        String s2 = "anavolimilovana";

        System.out.println(isPalindrome(s));
        System.out.println(isPalindrome2(s2));
        System.out.println(isPalindrome3(s2));
        System.out.println(isPalindrome4(s2));
        System.out.println(isPalindrome5(s2));

    }

    public static boolean isPalindrome(String string){
        char array[] = string.toCharArray();

        for (int i = array.length - 1, j = 0; i >= j; i--, j++){
            if (array[i] == array[j]){

            } else {
                return false;
            }
        }

        return true;

    }

    public static boolean isPalindrome2(String string){
        String reversed = "";

        for (int i = 0; i < string.length(); i++){
            reversed = reversed + string.charAt(string.length() - i - 1);
        }

        if (string.equals(reversed)){
            return true;
        }

        return false;
    }

    public static boolean isPalindrome3(String string){
        StringBuilder reversed = new StringBuilder();
        StringBuilder original = new StringBuilder(string);

        for (int i = 0; i < original.length(); i++){
        reversed = reversed.append(original.charAt(original.length() - i - 1));
        }

        if (reversed.toString().equals(original.toString())){
            return true;
        }

        return false;
    }

    public static boolean isPalindrome4(String string){

        StringBuilder original = new StringBuilder(string);
        original.reverse();


        if (original.toString().equals(string)){
            return true;
        }

        return false;
    }

    public static boolean isPalindrome5(String string){
        int i = 0;
        int j = string.length() - 1;
        int k = (i + j) / 2;

        for (int index = i; index <= k; index++){
            if (string.charAt(i) == string.charAt(j)){
                i++;
                j--;
            } else {
                return false;
            }
        }

        return true;
    }

}
