package com.stefana.commonquestions;

import java.util.Scanner;

public class TwoEggsProblem {

    public static void main(String[] args) {

     //   System.out.println("Minimum number of floors to find the critical floor is " + solve());

        whereItBrakes();
    }

    private static final int NUM_OF_EGGS = 2;
    private static final int NUM_OF_FLOORS = 100;
    private static int dpTable[][] = new int[NUM_OF_EGGS + 1][NUM_OF_FLOORS + 1];

    public static int solve(){

        // inicijalizacija 2D niza

        dpTable[0][0] = 1;
        dpTable[1][0] = 1;

        //linear search basically
        for (int i = 0; i <= NUM_OF_FLOORS; i++){
            dpTable[1][i] = i;

        }
        /////////////////////////////  tu se zavrsava inicijalizacija 2D niza


        //n is index for eggs
        //m is index for floors
        for (int n = 2; n <= NUM_OF_EGGS; n++){
            for (int m = 1; m <= NUM_OF_FLOORS; m++){

                dpTable[n][m] = Integer.MAX_VALUE;

                //check dropping the egg from 1 to the current floor
                for (int x = 1; x <= m; x++){

                    //we are finding worst case scenario
                    //we add 1 to include current drop
                    int maxDrops = 1 + Math.max(dpTable[n - 1][x - 1], dpTable[n][m - x]);
                                         //case where egg breaks       //egg doesn't break, m je uk br spratova, x trenutan sprat
                                  //n - 1 jer se jedno jaje razbilo
                                 //moramo da proverimo sve spratove ispod x

                    //this is taking the minimum (because we are after the minimum number of drops)
                    if (maxDrops < dpTable[n][m]){
                        dpTable[n][m] = maxDrops;
                    }
                }
            }
        }

        //the last item in the table contains the minimum number of drops
        return dpTable[NUM_OF_EGGS][NUM_OF_FLOORS];
    }

    public static void whereItBrakes(){

        int[] niz = new int[100];

        for (int i = 0; i <= niz.length; i = i+10){
            boolean b = doesItCrash(i);
            if (b){
                int floor = i - 10;
                for (int j = floor; j <= i; j++){
                    if (doesItCrash(j)){
                        System.out.println("Puca na " + j);
                        return;
                    }
                }
            }
        }
    }

    public static boolean doesItCrash(int floor){
        System.out.println(String.format("Floor is %d. Does it crash? y/n", floor));
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.next();
        return answer.equals("y");
    }
}
