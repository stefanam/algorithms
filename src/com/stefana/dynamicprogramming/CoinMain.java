package com.stefana.dynamicprogramming;

public class CoinMain {

    public static void main(String[] args) {

        int[] coins = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};  //v
        int totalAmount = 4000;  //M

        Coin coin = new Coin();
     //   System.out.println(coin.naiveChange(totalAmount, coins, 0));
        coin.dynamicCoinChange(coins, totalAmount);
    }
}
