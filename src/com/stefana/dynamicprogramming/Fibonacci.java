package com.stefana.dynamicprogramming;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

        Map<Integer, Integer> memorizeTable;

    public Fibonacci() {
        memorizeTable = new HashMap<>();
        this.memorizeTable.put(0, 0);
        this.memorizeTable.put(1, 1);
    }

   public int fibonacciDp(int n){

        if (memorizeTable.containsKey(n)) return memorizeTable.get(n);

        memorizeTable.put(n - 1, fibonacciDp(n - 1));
        memorizeTable.put(n - 2, fibonacciDp(n - 2));

        int calculatedNumber = memorizeTable.get(n - 1) + memorizeTable.get(n - 2);

        memorizeTable.put(n, calculatedNumber);
        return calculatedNumber;
   }

    //exponential running time
    public static int naiveFibonacci(int n){
        if (n == 0) return 0;
        if (n == 1) return 1;

        return naiveFibonacci(n - 1) + naiveFibonacci(n - 2);
    }

}
