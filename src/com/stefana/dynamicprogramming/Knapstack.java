package com.stefana.dynamicprogramming;

public class Knapstack {

    private int numOfItems;
    private int capacity;
    private int[][] table;
    private int[] weights;
    private int[] values;
    private int totalBenefit;

    public Knapstack(int numOfItems, int capacity, int[] wrights, int[] values) {
        this.numOfItems = numOfItems;
        this.capacity = capacity;
        this.weights = wrights;
        this.values = values;
        this.table = new int[numOfItems + 1][capacity + 1];
    }

    public void solve(){

        //time complexity: O(N * W)
        for (int i = 1; i < numOfItems + 1; i++){
            for (int j = 1; j < capacity + 1; j++){

                int notTakingItem = table[i - 1][j];
                int takingItem = 0;

                if (weights[i] <= j){
                    takingItem = values[i] + table[i - 1][j - weights[i]];
                }

                table[i][j] = Math.max(notTakingItem, takingItem);
            }
        }
        totalBenefit = table[numOfItems][capacity];


    }

    public void showResult(){
        System.out.println("Total benefit = " + totalBenefit);

        for (int n = numOfItems, w = capacity; n > 0; n--){
            if (table[n][w] != 0 && table[n][w] != table[n - 1][w]){
                System.out.println("We take item: #" + n);
                w = w - weights[n];
            }
        }
    }
}
