package com.stefana.dynamicprogramming;

public class KnapstackMain {

    public static void main(String[] args) {


        int numOfItems = 3;
        int capacity = 5;

        int[] weightsOfItems = new int[numOfItems + 1];
        weightsOfItems[1] = 4;
        weightsOfItems[2] = 2;
        weightsOfItems[3] = 3;

        int[] valuesOfItems = new int[numOfItems + 1];
        valuesOfItems[1] = 10;
        valuesOfItems[2] = 4;
        valuesOfItems[3] = 7;

        Knapstack knapstack = new Knapstack(numOfItems, capacity, weightsOfItems, valuesOfItems);
        knapstack.solve();
        knapstack.showResult();
    }
}
