package com.stefana.dynamicprogramming;

public class RodCutting {

    private int[][] dpTable;
    private int length;  //length of rod
    private int[] prices;

    public RodCutting(int length, int[] prices) {
        this.length = length;
        this.prices = prices;
        this.dpTable = new int[prices.length + 1][length + 1];
    }

    public void solve(){

        //initialize

        for (int i = 1; i < prices.length; i++){
            for (int j = 1; j <= length; j++){
                if (i <= j){
                    dpTable[i][j] = Math.max(dpTable[i - 1][j], prices[i] + dpTable[i - 1][j - i]);
                } else {
                    dpTable[i][j] = dpTable[i - 1][j];
                }
            }
        }
    }

    public void showResult(){
        System.out.println("Optimal profit: $" + dpTable[prices.length - 1][length]);

        for (int n = prices.length - 1, w = length; n > 0;){
            if (dpTable[n][w] != 0 && dpTable[n][w] != dpTable[n - 1][w]){

                System.out.println("We make cut: " + n + " m");
                w = w - n;

            }  else {
                n--;
            }
        }
    }
}
