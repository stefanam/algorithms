package com.stefana.dynamicprogramming;

public class RodCuttingMain {

    public static void main(String[] args) {

        int lengthOfRod = 5;
        int prices[] = {0, 2, 5, 7, 3};

        RodCutting rodCutting = new RodCutting(lengthOfRod, prices);

        rodCutting.solve();
        rodCutting.showResult();
    }
}
