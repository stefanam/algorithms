package com.stefana.recursiveanditerativeanghoritms;

public class AlghoritmMain {

    public static void main(String[] args) {

      //  Alghoritm alghoritm = new Alghoritm();
        //System.out.println(alghoritm.sumRecursive(3));

//        HouseBuildingProblem house = new HouseBuildingProblem();
//        System.out.println("Head recursion");
//        house.builtLayerHead(4);
//        System.out.println("Tail recursion");
//        house.builtLayerTail(4);

//        Factorial factorial = new Factorial();
//        System.out.println(factorial.factorial(5));
//        System.out.println(factorial.factorialIterative(5));
//        System.out.println(factorial.factorialWithAccomulator(1, 5));
 //       System.out.println(factorial.calculateFactorial(5));

        EuclidianAngoritm euclidianAngoritm = new EuclidianAngoritm();
        System.out.println(euclidianAngoritm.gcdRecursive(100, 30));
        System.out.println(euclidianAngoritm.gcdRecursive(30, 100));
        System.out.println(euclidianAngoritm.gcdIterative(30, 100));

    }
}
