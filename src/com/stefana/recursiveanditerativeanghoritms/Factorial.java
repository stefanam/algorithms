package com.stefana.recursiveanditerativeanghoritms;

public class Factorial {

    public int factorial(int n){
        // n! = n * (n - 1) * (n -2) ... * 1
        if (n == 1) {
            return 1;
        }
        return n * factorial(n-1);
    }

    public int factorialWithAccomulator(int accomulator, int n){
        if (n == 1){
            return accomulator;
        }

        return factorialWithAccomulator(accomulator * n, n - 1);
    }

    public int calculateFactorial(int n){
        return factorialWithAccomulator(1, n);
    }

    public int factorialIterative(int n){

       int result = 1;

        for (int i = 1; i <= n; i++){
           result = result * i;
        }

        return result;
    }
}
