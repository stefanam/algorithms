package com.stefana.recursiveanditerativeanghoritms;

public class HouseBuildingProblem {

    //head recursion
    public void builtLayerHead(int height){

        if (height == 0){
            return;
        }

        builtLayerHead(height - 1);
        System.out.println(height);
    }

    public void builtLayerTail(int height){

        if (height == 0){
            return;
        }
        System.out.println(height);
        builtLayerTail(height - 1);

    }
}
