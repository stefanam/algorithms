package com.stefana.recursiveanditerativeanghoritms.linearandbinarysearch;

public class LinearAndBinarySearch {

    private int[] array;

    public LinearAndBinarySearch(int[] array) {
        this.array = array;
    }

    public int linearSearch(int item){

        for (int i = 0; i < array.length; i++){
            if (array[i] == item){
                return i;
            }
        }

        return -1;
    }

    public int binarySearchRecursive(int startIndex, int endIndex, int item){

        if (startIndex == endIndex){
            System.out.println("Item is not present in the array...");
            return -1;
        }

        int middleIndex = (startIndex + endIndex) / 2;

        if (item == array[middleIndex]){
            return middleIndex;
        } else if (item < array[middleIndex]){
            return binarySearchRecursive(startIndex, middleIndex - 1, item);
        } else {
            return binarySearchRecursive(middleIndex + 1, endIndex, item);
        }
    }

    public int binarySearch(int item){
        return binarySearchRecursive(0, array.length - 1, item);
    }

    public int binarySearchIterative(int item){

        int start = 0;
        int end = array.length - 1;

        while (start <= end){

            int middle = (start + end) / 2;

            if (item < array[middle]){
                end = middle - 1;
            }

            if (item > array[middle]){
                start = middle + 1;
            }

            if (item == array[middle]){
                return middle;
            }
        }

        return -1;
    }
}
