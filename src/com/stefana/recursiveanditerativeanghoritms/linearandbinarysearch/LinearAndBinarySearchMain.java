package com.stefana.recursiveanditerativeanghoritms.linearandbinarysearch;

public class LinearAndBinarySearchMain {

    public static void main(String[] args) {

        int[] array = {1, 5, 3, 9, 8, 10, -3};
        LinearAndBinarySearch linearAndBinarySearch = new LinearAndBinarySearch(array);
        System.out.println(linearAndBinarySearch.linearSearch(10));

        int[] array2 = {2, 4, 6, 9, 88, 90, 123, 345};
        LinearAndBinarySearch linearAndBinarySearch1 = new LinearAndBinarySearch(array2);
        System.out.println(linearAndBinarySearch1.binarySearch(4));
        System.out.println(linearAndBinarySearch1.binarySearchIterative(4));
    }
}
