package com.stefana.recursiveanditerativeanghoritms.linearandbinarysearch.towersofhanoi;

public class HanoiMain {

    public static void main(String[] args) {

        TowerOfHanoi towerOfHanoi = new TowerOfHanoi();
        towerOfHanoi.solveHanoi(64, 'A', 'B', 'C');
    }
}
