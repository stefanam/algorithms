package com.stefana.recursiveanditerativeanghoritms.linearandbinarysearch.towersofhanoi;

public class TowerOfHanoi {

    //recursivly
    public void solveHanoi(int numberOfPlates, char rodFrom, char middleRod, char rodTo){

        //base case
        if(numberOfPlates == 1){
            System.out.println("Plate 1 from " + rodFrom + " to " + rodTo);
            return;
        }

        solveHanoi(numberOfPlates - 1, rodFrom, rodTo, middleRod);
        //moving n - 1 plates, from 1st to 3rd rod, with help of 2nd rod

        System.out.println("Plate " + numberOfPlates + " from " + rodFrom + " to " + rodTo);
        //moving largest plate to its final position (rod 3)

        solveHanoi(numberOfPlates - 1, middleRod, rodFrom, rodTo);
        //shift n - 1 plates from middle rod to the rodTo, with help of rodFrom
    }
}
