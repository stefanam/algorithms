package com.stefana.selection;

import java.util.Random;

public class QuickSelect {

    private int[] nums;

    public QuickSelect(int[] nums) {
        this.nums = nums;
    }

    public int select(int k){
        return select(0, nums.length - 1, k - 1);
    }

    private int select(int left, int right, int k) {

        int pivot = partition(left, right);

        if (pivot > k){

            return select(left, pivot - 1, k);

        } else if (pivot < k){

            select(pivot + 1, right, k);
        }
        return nums[k];
    }

    private int partition(int firstIndex, int lastIndex) {

        int pivot = new Random().nextInt(lastIndex - firstIndex + 1) + firstIndex;
        //generisemo pivot kao neki random broj

        swap(lastIndex, pivot);
        //menjamo posledjni index i pivot

        for (int i = firstIndex; i < lastIndex; i ++){
            //na pocetku je i prvi index
            if (nums[i] < nums[lastIndex]){   //ako trazimo najmanji element, stavljamo > ako trazimo najveci element
               //ako je i < poslednjeg indexa u nizu (sto je u ovom slucaju pivot) ulazimo u petlju
                swap(i, firstIndex);
                //menjamo i sa prvim indexom u nizu
                firstIndex++;
                //prvi index sada postaje prvi sledeci element u nizu posle njega
            }
        }

        swap(firstIndex, lastIndex);
        //ovom zamenom obezbedjujemo da ce svi elementi manji od pivota biti sa leve strane
        //a svi element veci od pivota biti sa desne strane
        return firstIndex;
    }

    private void swap(int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
