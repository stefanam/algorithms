package com.stefana.selection;

public class QuickSelectMain {

    public static void main(String[] args) {

        int[] nums = {1, 5, 4, 8, -2};

        QuickSelect quickSelect = new QuickSelect(nums);

        System.out.println(quickSelect.select(1));  //first smallest item, second smallest item = 1

        QuickSort q = new QuickSort(nums);
        q.sort(nums);
        q.print(nums);
    }
}
