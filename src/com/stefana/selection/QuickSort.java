package com.stefana.selection;

import java.util.Random;

public class QuickSort {

    private int[] numbers;

    public QuickSort(int[] numbers) {
        this.numbers = numbers;
    }

    public void sort(int[] array){
        quickSort(array, 0, array.length - 1);
    }

    private void quickSort(int[] array, int left, int right ) {

        int index = partition(array, left, right);

        if (left < index - 1){
            quickSort(array, left, index - 1);
        }

        if (index < right){
            quickSort(array, index, right);
        }
    }

    public int partition(int[] array, int left, int right){

        int pivot = array[(left + right) / 2];

        while (left <= right){

            while (array[left] < pivot){
                left++;
            }

            while (array[right] > pivot){
                right--;
            }

            if (left <= right){

                swap(left, right);
                left++;
                right--;
            }
        }

        return left;
    }

    private void swap(int i, int j){

        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }

    public void print(int[] arrray){

        for (int element : arrray){
            System.out.print(element + " ");
        }
    }
}
